package it.univet.visionar.libvarp;

import java.io.DataInputStream;


public class VisionARInputGetGyroscopeResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 13;

    private float gyroscopeX;
    private float gyroscopeY;
    private float gyroscopeZ;

    public VisionARInputGetGyroscopeResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public float getGyroscopeX() { return gyroscopeX; }
    public float getGyroscopeY() { return gyroscopeY; }
    public float getGyroscopeZ() { return gyroscopeZ; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_GYR_DATA)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            gyroscopeX = buffer.readFloat();
            gyroscopeY = buffer.readFloat();
            gyroscopeZ = buffer.readFloat();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            gyroscopeX = 0.0f;
            gyroscopeY = 0.0f;
            gyroscopeZ = 0.0f;
        }
    }
}
