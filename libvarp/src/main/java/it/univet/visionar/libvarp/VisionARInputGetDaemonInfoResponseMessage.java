package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

public class VisionARInputGetDaemonInfoResponseMessage extends VisionARInputMessage {

    private final static int expPayloadSize = 6;

    private int verMajor;
    private int verMinor;

    private int buildDay;
    private int buildMonth;
    private int buildYear;

    public VisionARInputGetDaemonInfoResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int getVerMajor() { return this.verMajor; };
    public int getVerMinor() { return this.verMinor; };

    public int getBuildDay() { return this.buildDay; };
    public int getBuildMonth() { return this.buildMonth; };
    public int getBuildYear() { return this.buildYear; };

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_PROT_INFO)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            verMajor = buffer.readByte();
            verMinor = buffer.readByte();

            buildDay = buffer.readByte();
            buildMonth = buffer.readByte();
            buildYear = buffer.readByte();
        }
        catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            verMajor = 0;
            verMinor = 0;
            buildDay = 0;
            buildMonth = 0;
            buildYear = 0;
        }
    }
}
