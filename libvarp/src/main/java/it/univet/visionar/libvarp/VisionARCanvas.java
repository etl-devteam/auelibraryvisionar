package it.univet.visionar.libvarp;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;


public class VisionARCanvas {
    private static final int WIDTH = 419;
    private static final int HEIGHT = 138;

    private String ipAddress;
    private int port;
    private Bitmap bitmap;
    private Canvas canvas;

    public VisionARCanvas(String addr, int port) {
        this.ipAddress = addr;
        this.port = port;

        this.bitmap = Bitmap.createBitmap(WIDTH, HEIGHT,Bitmap.Config.ARGB_8888);
        this.canvas = new Canvas(bitmap);
    }

    public VisionARCanvas() {
        this.ipAddress = VisionARMessage.defaultIpAddress;
        this.port = VisionARMessage.defaultTcpPort;

        this.bitmap = Bitmap.createBitmap(WIDTH, HEIGHT,Bitmap.Config.ARGB_8888);
        this.canvas = new Canvas(bitmap);
    }

    public void drawPaint(Paint paint) {
        canvas.drawPaint(paint);
    }

    public void drawText (String text, float x, float y, Paint paint) {
        canvas.drawText(text, x, y, paint);
    }

    public void drawBitmap (Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public void draw() {
        try {
            VisionAROutputSetFBMessage message = new VisionAROutputSetFBMessage(bitmap);
            VisionARClientTask task = new VisionARClientTask();
            task.execute( new VisionARClientTaskParams(this.ipAddress, this.port, message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getWidth() { return WIDTH; }

    public int getHeight() { return HEIGHT; }
}
