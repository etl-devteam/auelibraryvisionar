package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetHapticMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 2;

    public VisionAROutputSetHapticMessage(int duration) {
        super(SET_HAPTIC, TYPE_REQUEST);

        /* Check arguments */
        if ((duration < 0) || (duration > 60000))
            throw new IllegalArgumentException("Duration setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.writeShort(duration);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
