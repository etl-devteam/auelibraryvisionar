package it.univet.visionar.libvarp;

public class VisionARInputMessageException extends Exception {
    public String reason;

    VisionARInputMessageException(String reason) {
        this.reason = reason;
    }
}
