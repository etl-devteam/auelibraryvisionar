package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetCULedsStatusResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 5;

    private int gsm;
    private int wifi;
    private int bt;
    private int status;

    public VisionARInputGetCULedsStatusResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int getGsmLed() { return gsm; }
    public int getWifiLed() { return wifi; }
    public int getBtLed() { return bt; }
    public int getStatusLed() { return status; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CU_LEDS_STATUS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            gsm = buffer.readUnsignedByte();
            wifi = buffer.readUnsignedByte();
            bt = buffer.readUnsignedByte();
            status = buffer.readUnsignedByte();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            gsm = 0;
            wifi = 0;
            bt = 0;
            status = 0;
        }
    }
}
