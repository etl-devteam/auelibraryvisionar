package it.univet.visionar.libvarp;

import android.graphics.Bitmap;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;

public class VisionAROutputSetFBMessage extends VisionAROutputMessage {

    public VisionAROutputSetFBMessage(Bitmap bmp) {
        super(SET_FB, TYPE_REQUEST);

        final int lnth=bmp.getByteCount();
        ByteBuffer dst= ByteBuffer.allocate(lnth);
        bmp.copyPixelsToBuffer(dst);

        this.setPayload(new ByteArrayInputStream(dst.array()), lnth);
    }
}
