package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetDisplayRefreshTimeMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 2;

    public VisionAROutputSetDisplayRefreshTimeMessage(int time) {
        super(SET_LCD_REFRESH, TYPE_REQUEST);

        /* Check arguments */
        if ((time < 0) || (time > 60000))
            throw new IllegalArgumentException("Refresh time setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.writeShort(time);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
