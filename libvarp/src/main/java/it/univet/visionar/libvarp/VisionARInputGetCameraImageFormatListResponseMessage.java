package it.univet.visionar.libvarp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.DataInputStream;
import java.nio.ByteBuffer;


public class VisionARInputGetCameraImageFormatListResponseMessage extends VisionARInputMessage {
    private final static int expMinPayloadSize = 3;

    private short numberOfFormats;

    private short ids[];
    private int formats[];
    private short widths[];
    private short heights[];

    public VisionARInputGetCameraImageFormatListResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public short getNumberOfFormats() { return numberOfFormats; }

    public short getIdByIndex(int i) {
        if (i < numberOfFormats) return ids[i];

        throw new IllegalArgumentException("Index too big");
    }
    public int getFormatByIndex(int i) {
        if (i < numberOfFormats) return formats[i];

        throw new IllegalArgumentException("Index too big");
    }
    public short getWidthByIndex(int i) {
        if (i < numberOfFormats) return widths[i];

        throw new IllegalArgumentException("Index too big");
    }
    public short getHeightByIndex(int i) {
        if (i < numberOfFormats) return heights[i];

        throw new IllegalArgumentException("Index too big");
    }

    public int getFormatById(short i) { return formats[getIndexById(i)]; }
    public short getWidthById(short i) { return widths[getIndexById(i)]; }
    public short getHeightById(short i) { return heights[getIndexById(i)]; }

    @Override
    protected boolean checkHeader() {
        if ( (getType() == TYPE_RESPONSE)
                && (getCommand() == GET_CAM_IMG_LIST)
                && (getPayloadSize() > expMinPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            numberOfFormats = buffer.readShort();
            ids = new short[numberOfFormats];
            formats = new int[numberOfFormats];
            widths = new short[numberOfFormats];
            heights = new short[numberOfFormats];

            for (int i = 0; i < numberOfFormats; i++) {
                ids[i] = buffer.readShort();
                formats[i] = buffer.readInt();
                widths[i] = buffer.readShort();
                heights[i] = buffer.readShort();
            }
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            numberOfFormats = 0;
            ids = null;
            formats = null;
            widths = null;
            heights = null;
        }
    }

    private int getIndexById(short id) {
        for (int i = 0; i < numberOfFormats; i++) {
            if (ids[i] == id) {
                return i;
            }
        }

        throw new IllegalArgumentException("Id not found");
    }
}
