package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetCameraBrightnessResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 5;

    private int value;

    public VisionARInputGetCameraBrightnessResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int get() { return value; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CAM_BRIGHTNESS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            value = buffer.readInt();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            value = 0;
        }
    }
}
