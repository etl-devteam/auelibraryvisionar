package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetDisplayLockStatusResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 2;

    private boolean lock;

    public VisionARInputGetDisplayLockStatusResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public boolean get() { return lock; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_LOCK_STATUS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            lock = buffer.readBoolean();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            lock = false;
        }
    }
}
