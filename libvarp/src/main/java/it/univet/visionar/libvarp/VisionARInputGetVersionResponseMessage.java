package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

public class VisionARInputGetVersionResponseMessage extends VisionARInputMessage {
    private final static int expMinPayloadSize = 12;

    private int protocolMajorVersion;
    private int protocolMinorVersion;

    private int bootMajorVersion;
    private int bootMinorVersion;

    private int applicativeMajorVersion;
    private int applicativeMinorVersion;
    private int applicativePatchVersion;
    private int applicativeDay;
    private int applicativeMonth;
    private int applicativeYear;

    private int hwRevision;

    private String spectType;

    public VisionARInputGetVersionResponseMessage(VisionARInputMessage msg) { super(msg); }

    public int getProtocolMajorVersion() { return protocolMajorVersion; }
    public int getProtocolMinorVersion() { return protocolMinorVersion; }
    public int getBootMajorVersion() { return bootMajorVersion; }
    public int getBootMinorVersion() { return bootMinorVersion; }
    public int getApplicativeMajorVersion() { return applicativeMajorVersion; }
    public int getApplicativeMinorVersion() { return applicativeMinorVersion; }
    public int getApplicativePatchVersion() { return applicativePatchVersion; }
    public int getApplicativeDay() { return applicativeDay; }
    public int getApplicativeMonth() { return applicativeMonth; }
    public int getApplicativeYear() { return applicativeYear; }
    public int getHwRevision() { return hwRevision; }
    public String getSpectaclesType() { return spectType; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_VERSION)
                && (size >= expMinPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            protocolMajorVersion = buffer.readUnsignedByte();
            protocolMinorVersion = buffer.readUnsignedByte();
            bootMajorVersion = buffer.readUnsignedByte();
            bootMinorVersion = buffer.readUnsignedByte();
            applicativeMajorVersion = buffer.readUnsignedByte();
            applicativeMinorVersion = buffer.readUnsignedByte();
            applicativePatchVersion = buffer.readUnsignedByte();
            applicativeDay = buffer.readUnsignedByte();
            applicativeMonth = buffer.readUnsignedByte();
            applicativeYear = buffer.readUnsignedByte();
            hwRevision = buffer.readUnsignedByte();
            spectType = buffer.readLine();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            protocolMajorVersion = 0;
            protocolMinorVersion = 0;
            bootMajorVersion = 0;
            bootMinorVersion = 0;
            applicativeMajorVersion = 0;
            applicativeMinorVersion = 0;
            applicativePatchVersion = 0;
            applicativeDay = 0;
            applicativeMonth = 0;
            applicativeYear = 0;
            hwRevision = 0;
            spectType = null;
        }
    }
}
