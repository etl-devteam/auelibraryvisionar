package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetCUGlassesEnableMessage extends VisionAROutputMessage {
    private final static int expPayloadSize = 1;

    public VisionAROutputSetCUGlassesEnableMessage(boolean enable) {
        super(SET_CU_GLASS_ENABLE, TYPE_REQUEST);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.writeBoolean(enable);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
