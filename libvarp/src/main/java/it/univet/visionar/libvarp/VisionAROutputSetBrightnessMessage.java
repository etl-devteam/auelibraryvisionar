package it.univet.visionar.libvarp;

public class VisionAROutputSetBrightnessMessage extends VisionAROutputMessage {

    public VisionAROutputSetBrightnessMessage(int bright) {
        super(SET_BRIGHTNESS, TYPE_REQUEST);

        if ((bright < 0) || (bright > 255))
            throw new IllegalArgumentException("Brightness setting is out of range");

        this.setByteAsPayload((byte)bright);
    }
}
