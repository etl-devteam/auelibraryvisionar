package it.univet.visionar.libvarp;

public class VisionARMessage {
    /* Tag for logging */
    protected static final String TAG = "VARP_MSG";


    /* Header v.1 layout */
    protected static final int VERSION_HEADER_OFFSET    =   0;
    protected static final int TYPE_HEADER_OFFSET       =   1;
    protected static final int COMMAND_HEADER_OFFSET    =   2;
    protected static final int SIZE_HEADER_OFFSET       =   4;
    protected static final int HEADER_SIZE              =   8;
    protected static final int PAYLOAD_OFFSET           =   HEADER_SIZE;


    /* Message Type */
    public static final byte TYPE_REQUEST               =   1;
    public static final byte TYPE_RESPONSE              =   2;
    public static final byte TYPE_NOTIFICATION          =   3;

    /* Response Type */
    public static final byte OK_RESPONSE                =   1;
    public static final byte KO_RESPONSE                =   0;

    /* Commands */
    /* Daemon Info */
    public static final short GET_DAEMON_STATUS         =   0;
    public static final short GET_PROT_INFO             =   1;
    public static final short GET_CLIENTS_INFO          =   2;
    public static final short GET_LOG_LEVEL             =   3;

    /* Control Unit Info */
    public static final short GET_CU_INFO               =  11;
    public static final short GET_CU_BATT_STATUS        =  13;
    public static final short GET_CU_GLASS_ENABLE       =  15;
    public static final short GET_CU_LEDS_STATUS        =  17;

    /* Spectacles Info */
    public static final short GET_CONN_STATUS           = 100;
    public static final short GET_VERSION               = 101;
    public static final short GET_STATUS                = 102;
    public static final short GET_MAG_DATA              = 103;
    public static final short GET_GYR_DATA              = 104;
    public static final short GET_COMP_DATA             = 105;
    public static final short GET_ACC_DATA              = 106;
    public static final short GET_LEDS_STATUS           = 107;
    public static final short GET_BRIGHTNESS            = 108;
    public static final short GET_CONTRAST              = 109;
    public static final short GET_ASL                   = 110;
    public static final short GET_I2C_DATA              = 111;
    public static final short GET_LCD_REFRESH           = 112;
    public static final short GET_LOCK_STATUS           = 113;

    /* Camera Info */
    public static final short GET_CAM_INFO              = 151;
    public static final short GET_CAM_IMG_LIST          = 153;
    public static final short GET_CAM_IMG               = 154;
    public static final short GET_CAM_STREAM_ENABLE     = 155;
    public static final short GET_CAM_CAPTURE           = 156;
    public static final short GET_CAM_LED_STATUS        = 157;
    public static final short GET_CAM_BRIGHTNESS        = 158;
    public static final short GET_CAM_CONTRAST          = 159;

    /* Daemon Settings */
    public static final short SET_LOG_LEVEL             = 203;

    /* Control Unit Settings */
    public static final short SET_CU_BATT_STATUS        = 213;
    public static final short SET_CU_GLASS_ENABLE       = 215;
    public static final short SET_CU_LEDS_STATUS        = 217;

    /* Spectacles Settings */
    public static final short SET_HAPTIC                = 301;
    public static final short SET_FB                    = 305;
    public static final short SET_OBJS                  = 306;
    public static final short SET_LEDS_STATUS           = 307;
    public static final short SET_BRIGHTNESS            = 308;
    public static final short SET_CONTRAST              = 309;
    public static final short SET_I2C_DATA              = 311;
    public static final short SET_LCD_REFRESH           = 312;
    public static final short SET_LOCK_STATUS           = 313;

    /* Camera Settings */
    public static final short SET_CAM_IMG               = 354;
    public static final short SET_CAM_STREAM_ENABLE     = 355;
    public static final short SET_CAM_LED_STATUS        = 357;
    public static final short SET_CAM_BRIGHTNESS        = 358;
    public static final short SET_CAM_CONTRAST          = 359;

    /* Daemon Notifications */
    public static final short NTF_DMN_STATUS            = 600;

    /* Control Unit Notifications */
    public static final short NTF_CU_KEY                = 611;

    /* Spectacles Notifications */
    public static final short NTF_CONN                  = 700;
    public static final short NTF_KEY                   = 701;
    public static final short NTF_DISPLAY_LOCK          = 713;

    /* Camera Notifications */
    public static final short NTF_CAM_CAPTURE           = 756;

    /* reserved */
    public static final short SET_FB_FORCED             =1305;

    /* Default network parameters */
    public static final String defaultIpAddress        = "127.0.0.1";
    public static final int defaultTcpPort             = 5000;
    public static final int defaultTcpEvtPort          = 5001;

    /* Control unit input keys */
    public static final short DOWN_BUTTON_KEY           = 257;  // DOWN/LEFT/BACK button
    public static final short ENTER_BUTTON_KEY          = 258;  // ENTER/OK button
    public static final short UP_BUTTON_KEY             = 259;  // UP/RIGHT/FWD button
    public static final short ESC_BUTTON_KEY            = 260;  // ESC button
    /* Spectacles input keys */
    public static final short SINGLE_BUTTON1_KEY        =  59; // tap on button 1
    public static final short SINGLE_BUTTON2_KEY        =  60; // tap on button 2
    public static final short SINGLE_BUTTON3_KEY        =  61; // tap on button 3
    public static final short SINGLE_BUTTON4_KEY        =  62; // tap on button 4
    public static final short DOUBLE_BUTTON1_KEY        =  63; // double tap on button 1
    public static final short DOUBLE_BUTTON2_KEY        =  64; // double tap on button 2
    public static final short DOUBLE_BUTTON3_KEY        =  65; // double tap on button 3
    public static final short DOUBLE_BUTTON4_KEY        =  66; // double tap on button 4
    public static final short SWIPE_UP_KEY              =  67; // swipe rear to front
    public static final short SWIPE_DOWN_KEY            =  68; // swipe front to rear
    /* Buttons values */
    public static final int RELEASED_BUTTON             =   0;
    public static final int PRESSED_BUTTON              =   1;

    /* Image formats */
    public static final int YUYV_IMAGE                  = 0x56595559;   // MJPEG
    public static final int MJPG_IMAGE                  = 0x47504A4D;   // YUV 4:2:2

    /* Control Unit LEDs */
    public static final int CU_LED_UNDEF                = 255; // no change of LED status
    public static final int CU_LED_OFF                  =   0; // LED off
    public static final int CU_LED_ON                   =   1; // LED on (for all the LEDs but RGB)
    public static final int CU_LED_GREEN                =   1; /* (only for RGB LED) */
    public static final int CU_LED_RED                  =   2; /* (only for RGB LED) */
    public static final int CU_LED_ORANGE               =   3; /* (only for RGB LED) */
    public static final int CU_LED_BLUE                 =   4; /* (only for RGB LED) */
    public static final int CU_LED_AZURE                =   5; /* (only for RGB LED) */
    public static final int CU_LED_PURPLE               =   6; /* (only for RGB LED) */
    public static final int CU_LED_VIOLET               =   7; /* (only for RGB LED) */


    /* Camera LED */
    public static final int CAMERA_LED_OFF              = 0;
    public static final int CAMERA_LED_RED              = 1;
    public static final int CAMERA_LED_GREEN            = 2;

}
