package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetLedsStatusResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 4;

    private int red;
    private int green;
    private int blue;

    public VisionARInputGetLedsStatusResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int getRedLed() { return red; }
    public int getGreenLed() { return green; }
    public int getBlueLed() { return blue; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_LEDS_STATUS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            red = buffer.readUnsignedByte();
            green = buffer.readUnsignedByte();
            blue = buffer.readUnsignedByte();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            red = 0;
            green = 0;
            blue = 0;
        }
    }
}
