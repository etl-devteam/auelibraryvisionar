package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetCameraContrastMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 4;

    public VisionAROutputSetCameraContrastMessage(int contr) {
        super(SET_CAM_CONTRAST, TYPE_REQUEST);

        if ((contr < 0) || (contr > 255))
            throw new IllegalArgumentException("Contrast setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.writeInt(contr);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
