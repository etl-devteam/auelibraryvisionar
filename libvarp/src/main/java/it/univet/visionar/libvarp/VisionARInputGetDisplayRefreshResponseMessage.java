package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetDisplayRefreshResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 3;

    private short value;

    public VisionARInputGetDisplayRefreshResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public short get() { return value; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_LCD_REFRESH)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            value = buffer.readShort();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            value = 0;
        }
    }
}
