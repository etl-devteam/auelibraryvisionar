package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetStatusResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 5;

    private int result;

    public VisionARInputGetStatusResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int get() { return result; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_STATUS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            result = buffer.readInt();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            result = 0;
        }
    }
}
