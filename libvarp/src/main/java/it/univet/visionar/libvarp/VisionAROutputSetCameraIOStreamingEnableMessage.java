package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetCameraIOStreamingEnableMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 1;

    public VisionAROutputSetCameraIOStreamingEnableMessage(boolean enable) {
        super(SET_CAM_STREAM_ENABLE, TYPE_REQUEST);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.writeBoolean(enable);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
