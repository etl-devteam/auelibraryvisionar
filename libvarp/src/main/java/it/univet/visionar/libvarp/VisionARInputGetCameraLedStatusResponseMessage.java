package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetCameraLedStatusResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 2;

    private int status;

    public VisionARInputGetCameraLedStatusResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int get() { return status; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CAM_LED_STATUS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            status = buffer.readUnsignedByte();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            status = 0;
        }
    }
}
