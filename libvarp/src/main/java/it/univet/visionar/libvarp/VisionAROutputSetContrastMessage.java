package it.univet.visionar.libvarp;

public class VisionAROutputSetContrastMessage extends VisionAROutputMessage {

    public VisionAROutputSetContrastMessage(int contr) {
        super(SET_CONTRAST, TYPE_REQUEST);

        if ((contr < 0) || (contr > 255))
            throw new IllegalArgumentException("Contrast setting is out of range");

        this.setByteAsPayload((byte)contr);
    }
}
