package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

public class VisionARInputMessage extends VisionARMessage {
    protected byte version;
    protected byte type;
    protected short cmd;
    protected int size;
    protected byte response;
    protected byte[] payload;

    public VisionARInputMessage() {
        reset();
    }

    public VisionARInputMessage(DataInputStream msg) {
        read(msg);
    }

    public VisionARInputMessage(VisionARInputMessage msg) {
        version = msg.getVersion();
        type = msg.getType();
        cmd = msg.getCommand();
        size = msg.getPayloadSize();
        response = msg.getResponse();
        payload = msg.getPayload();
        if (payload != null) {
            readData(new DataInputStream(new ByteArrayInputStream(payload)));
        }

        if ( !checkHeader() ) {
            response = KO_RESPONSE;
        }
    }

    public void reset () {
        version = 0;
        type = 0;
        cmd = 0;
        size = 0;
        response = KO_RESPONSE;
        payload = null;
    }

    public byte getVersion() { return version; }
    public byte getType() { return type; }
    public short getCommand() { return cmd; }
    public byte getResponse() { return response; }
    public int getPayloadSize() { return size; }
    public byte[] getPayload() { return payload; }

    public byte read (DataInputStream msg) {
        int bytesRead = 0;

        try {
            version = msg.readByte();
            if (version == 1) {
                type = msg.readByte();
                cmd = msg.readShort();
                size = msg.readInt();

                VisionARLog.i(TAG, "type = " + type + "; cmd = " + cmd + "; size = " + size);

                if (size > 0) {
                    if (type != TYPE_NOTIFICATION) {
                        response = msg.readByte();
                        if (size > 1) {
                            payload = new byte[size - 1];
                            do {
                                bytesRead += msg.read(payload, bytesRead, size-1-bytesRead);
                            } while (bytesRead < size - 1);
                        } else {
                            payload = null;
                        }
                    } else {
                        response = OK_RESPONSE;
                        payload = new byte[size];
                        do {
                            bytesRead += msg.read(payload, bytesRead, size-bytesRead);
                        } while (bytesRead < size);
                    }
                } else {
                    response = KO_RESPONSE;
                }
            }
            else {
                VisionARLog.e(TAG, "wrong version!");

                type = 0;
                cmd = 0;
                size = 0;
                response = KO_RESPONSE;
                payload = null;
            }
        }
        catch (Exception e) {
            size = 0;
            response = KO_RESPONSE;
        }

        return response;
    }

    protected boolean checkHeader() {
        VisionARLog.e(TAG, "unexpected call!");

        return false;
    }

    protected void readData(DataInputStream buffer) {
        VisionARLog.e(TAG, "unexpected call!");
    }
}
