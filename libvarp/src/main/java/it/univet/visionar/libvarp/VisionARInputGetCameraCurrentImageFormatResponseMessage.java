package it.univet.visionar.libvarp;

import java.io.DataInputStream;


public class VisionARInputGetCameraCurrentImageFormatResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 11;

    private short id;
    private int format;
    private short width;
    private short height;

    public VisionARInputGetCameraCurrentImageFormatResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public short getId() { return id; }
    public int getFormat() { return format; }
    public short getWidth() { return width; }
    public short getHeight() { return height; }

    @Override
    protected boolean checkHeader() {
        if ( (getType() == TYPE_RESPONSE)
                && (getCommand() == GET_CAM_IMG)
                && (getPayloadSize() == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            id = buffer.readShort();
            format = buffer.readInt();
            width = buffer.readShort();
            height = buffer.readShort();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            id = 0;
            format = 0;
            width = 0;
            height = 0;
        }
    }
}
