package it.univet.visionar.libvarp;

import android.graphics.Bitmap;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;

public class VisionAROutputSetFBForcedMessage extends VisionAROutputMessage {

    public VisionAROutputSetFBForcedMessage(Bitmap bmp) {
        super(SET_FB_FORCED, TYPE_REQUEST);

        final int lnth=bmp.getByteCount();
        ByteBuffer dst= ByteBuffer.allocate(lnth);
        bmp.copyPixelsToBuffer(dst);

        this.setPayload(new ByteArrayInputStream(dst.array()), lnth);
    }
}
