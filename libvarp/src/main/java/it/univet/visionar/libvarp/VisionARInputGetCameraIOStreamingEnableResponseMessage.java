package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetCameraIOStreamingEnableResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 2;

    private boolean enable;

    public VisionARInputGetCameraIOStreamingEnableResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public boolean getEnable() { return enable; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CAM_STREAM_ENABLE)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            enable = buffer.readBoolean();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            enable = false;
        }
    }
}
