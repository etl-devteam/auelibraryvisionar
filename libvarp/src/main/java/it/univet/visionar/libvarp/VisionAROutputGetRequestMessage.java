package it.univet.visionar.libvarp;

public class VisionAROutputGetRequestMessage extends VisionAROutputMessage {

    public VisionAROutputGetRequestMessage(short cmd) {
        super(cmd, TYPE_REQUEST);
    }
}
