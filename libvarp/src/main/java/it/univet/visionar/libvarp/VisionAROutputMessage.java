package it.univet.visionar.libvarp;

import java.io.DataOutputStream;
import java.io.InputStream;

public class VisionAROutputMessage extends VisionARMessage {
    private byte type;
    private short cmd;
    private int size = 0;
    private byte[] payload = null;

    public VisionAROutputMessage(short cmd, byte type) {
        this.type = type;
        this.cmd = cmd;
    }

    public VisionAROutputMessage(short cmd, byte type, int size, InputStream payload) {
        this.type = type;
        this.cmd = cmd;
        this.setPayload(payload, size);
    }

    public void setPayload(InputStream payload, int size) {
        this.size = size;
        this.payload = new byte[size];

        try {
            payload.read(this.payload, 0, size);
        }
        catch (Exception e) {

        }
    }

    public void setByteAsPayload(byte val) {
        this.size = 1;
        this.payload = new byte[size];

        this.payload[0] = val;
    }

    public void writeTo (DataOutputStream output) {
        try {
            output.write(1);    /* Version */
            output.write(type);
            output.writeShort(cmd);
            output.writeInt(size);
            if (size > 0) {
                output.write(payload, 0, size);
            }
        }
        catch (Exception e) {

        }
    }
}
