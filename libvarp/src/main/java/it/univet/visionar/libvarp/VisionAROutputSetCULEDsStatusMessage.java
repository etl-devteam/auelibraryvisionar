package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetCULEDsStatusMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 4;

    public VisionAROutputSetCULEDsStatusMessage(int gsm, int wifi, int bt, int rgb) {
        super(SET_CU_LEDS_STATUS, TYPE_REQUEST);

        /* Check arguments */
        if ((gsm != CU_LED_OFF) && (gsm != CU_LED_ON) && (gsm != CU_LED_UNDEF))
            throw new IllegalArgumentException("GSM LED setting is out of range");
        if ((wifi != CU_LED_OFF) && (wifi != CU_LED_ON) && (wifi != CU_LED_UNDEF))
            throw new IllegalArgumentException("Wi-Fi LED setting is out of range");
        if ((bt != CU_LED_OFF) && (bt != CU_LED_ON) && (bt != CU_LED_UNDEF))
            throw new IllegalArgumentException("BT LED setting is out of range");
        if ((rgb < CU_LED_OFF) || ((rgb > CU_LED_VIOLET) && (rgb != CU_LED_UNDEF)))
            throw new IllegalArgumentException("RGB LED setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.write(gsm);
            stream.write(wifi);
            stream.write(bt);
            stream.write(rgb);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
