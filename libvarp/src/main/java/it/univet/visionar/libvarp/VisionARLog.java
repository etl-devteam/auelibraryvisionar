package it.univet.visionar.libvarp;

import android.util.Log;

public class VisionARLog {

    public static int v(String tag, String msg) {
        return Log.v(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]: " + msg);
    }

    public static int v(String tag) {
        return Log.v(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]");
    }

    public static int d(String tag, String msg) {
        return Log.d(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]: " + msg);
    }

    public static int d(String tag) {
        return Log.d(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]");
    }

    public static int i(String tag, String msg) {
        return Log.i(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]: " + msg);
    }

    public static int i(String tag) {
        return Log.i(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]");
    }

    public static int w(String tag, String msg) {
        return Log.w(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]: " + msg);
    }

    public static int w(String tag) {
        return Log.w(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]");
    }

    public static int e(String tag, String msg) {
        return Log.e(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]: " + msg);
    }

    public static int e(String tag) {
        return Log.e(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]");
    }

    public static int wtf(String tag, String msg) {
        return Log.wtf(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]: " + msg);
    }

    public static int wtf(String tag) {
        return Log.wtf(tag, "[" + new Throwable().getStackTrace()[1].getFileName() + ":" + new Throwable().getStackTrace()[1].getLineNumber() +"]");
    }
}
