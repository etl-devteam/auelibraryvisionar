package it.univet.visionar.libvarp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.appcompat.app.AppCompatActivity;

public class VisionARActivity extends AppCompatActivity {
    private static final String TAG = "VARP_ACT";

    private static VisionARClientNotificationTask mNtfTask = null;
    private static Handler mHandler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mHandler == null) {
            mHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == VisionARMessage.TYPE_NOTIFICATION) {
                        VisionARInputMessage input = (VisionARInputMessage) msg.obj;
                        switch (input.getCommand()) {
                            /* Control Unit Notifications */
                            case VisionARMessage.NTF_CU_KEY:
                                onControlUnitKeyReceived(new VisionARInputKeyNotification(input));
                                break;

                            /* Spectacles Notifications */
                            case VisionARMessage.NTF_CONN:
                                onConnectionReceived(new VisionARConnectionStatusNotification(input));
                                break;
                            case VisionARMessage.NTF_KEY:
                                onSpectaclesKeyReceived(new VisionARInputKeyNotification(input));
                                break;
                            case VisionARMessage.NTF_DISPLAY_LOCK:
                                onSpectaclesDisplayLockChangedReceived(new VisionARDisplayLockChangedNotification(input));
                                break;

                            /* Camera Notifications */
                            case VisionARMessage.NTF_CAM_CAPTURE:
                                onCameraImageReceived(new VisionARCameraCapturingNotification(input));
                                break;

                            default:
                                break;
                        }
                    }
                }
            };
        }

        if (mNtfTask == null) {
            mNtfTask = new VisionARClientNotificationTask() {
                @Override
                protected void update(VisionARInputMessage message) {
                    if (message.getType() == VisionARMessage.TYPE_NOTIFICATION) {
                        VisionARLog.v(TAG, "notification received: " + message.getCommand());

                        Message msg = Message.obtain(mHandler, message.getType(), message);
                        msg.sendToTarget();
                    }
                }
            };
            Thread mNtfThread = new Thread(mNtfTask);
            mNtfThread.start();
        }
    }

    protected void onControlUnitKeyReceived(VisionARInputKeyNotification msg) {
        VisionARLog.i(TAG, msg.getCode() + " -> " + msg.getValue());
    }

    protected void onSpectaclesKeyReceived(VisionARInputKeyNotification msg) {
        VisionARLog.i(TAG, msg.getCode() + " -> " + msg.getValue());
    }

    protected void onConnectionReceived(VisionARConnectionStatusNotification msg) {
        VisionARLog.i(TAG, "" + msg.getStatus());
    }

    protected void onSpectaclesDisplayLockChangedReceived(VisionARDisplayLockChangedNotification msg) {
        VisionARLog.i(TAG, "" + msg.getStatus());
    }

    protected void onCameraImageReceived(VisionARCameraCapturingNotification msg) {
        VisionARLog.i(TAG, msg.getFormat() + "(" + msg.getWidth() + "x" + msg.getHeight() + ")");
    }
}
