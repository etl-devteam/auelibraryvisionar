package it.univet.visionar.libvarp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;


public class VisionARInputGetCameraCaptureResponseMessage extends VisionARInputMessage {
    private final int minPayloadSize = 9;

    private int format;
    private int width;
    private int height;
    private Bitmap bitmap;

    public VisionARInputGetCameraCaptureResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int getFormat() { return format; }
    public int getWidth() { return width; }
    public int getHeight() { return height; }
    public Bitmap getBitmap() { return bitmap; }

    @Override
    protected boolean checkHeader() {
        if ( (getType() == TYPE_RESPONSE)
                && (getCommand() == GET_CAM_CAPTURE)
                && (getPayloadSize() >= minPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            byte inImg[] = new byte[getPayloadSize() - 9];

            format = buffer.readInt();
            width = buffer.readShort();
            height = buffer.readShort();

            buffer.read(inImg, 0, inImg.length);

            VisionARLog.d(TAG, String.format("0x%08X", format) + " " + width + "x" + height + " -> " + inImg.length);

            if (format == YUYV_IMAGE) {
                VisionARLog.d(TAG, "YUYV");

                byte outImg[] = new byte[4*width*height];

                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j += 2) {
                        int u = (inImg[1 + 2*(i*width+j)] & 0xFF);
                        int v = (inImg[3 + 2*(i*width+j)] & 0xFF);
                        int u1 = (((u - 128) << 7) + (u - 128)) >> 6;
                        int rg = (((u - 128) << 1) + (u - 128) + ((v - 128) << 2) + ((v - 128) << 1)) >> 3;
                        int v1 = (((v - 128) << 1) + (v - 128)) >> 1;

                        outImg[0+4*(i*width+j)] = CLIP((inImg[0 + 2*(i*width+j)] & 0xFF) + v1);
                        outImg[1+4*(i*width+j)] = CLIP((inImg[0 + 2*(i*width+j)] & 0XFF) - rg);
                        outImg[2+4*(i*width+j)] = CLIP((inImg[0 + 2*(i*width+j)] & 0xFF) + u1);
                        outImg[3+4*(i*width+j)] = (byte)0xFF;
                        outImg[4+4*(i*width+j)] = CLIP((inImg[2 + 2*(i*width+j)] & 0xFF) + v1);
                        outImg[5+4*(i*width+j)] = CLIP((inImg[2 + 2*(i*width+j)] & 0xFF) - rg);
                        outImg[6+4*(i*width+j)] = CLIP((inImg[2 + 2*(i*width+j)] & 0xFF) + u1);
                        outImg[7+4*(i*width+j)] = (byte)0xFF;
                    }
                }

                ByteBuffer dataImg = ByteBuffer.wrap(outImg);
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                bitmap.copyPixelsFromBuffer(dataImg);
            }
            else {
                // JPEG
                VisionARLog.d(TAG, "MJPG");
                bitmap = BitmapFactory.decodeByteArray(inImg, 0,inImg.length);
            }
        } catch (Exception e) {
            format = 0;
            width = 0;
            height = 0;
            bitmap = null;
        }
    }

    private static byte CLIP (int val) {
        if (val > 0xFF)
            return (byte)0xFF;
        if (val < 0)
            return (byte)0;
        return (byte)val;
    }
}
