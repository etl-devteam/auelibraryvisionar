package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputSetResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 1;

    private short expCmd;

    public VisionARInputSetResponseMessage(short cmd, VisionARInputMessage msg) {
        super(msg);
        if (this.cmd != cmd) {
            response = KO_RESPONSE;
        }
    }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE) && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        VisionARLog.e(TAG, "No other data to read!");
    }
}
