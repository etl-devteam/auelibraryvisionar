package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetCameraInfoResponseMessage extends VisionARInputMessage {
    private final static int expMinPayloadSize = 4;

    private int major;
    private int minor;
    private int patch;

    private String name;

    public VisionARInputGetCameraInfoResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int getMajorRelease() { return major; }
    public int getMinorRelease() { return minor; }
    public int getPatchNumber() { return patch; }
    public String getName() { return name; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CAM_INFO)
                && (size >= expMinPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            major = buffer.readUnsignedByte();
            minor = buffer.readUnsignedByte();
            patch = buffer.readUnsignedByte();
            name = buffer.readLine();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            major = 0;
            minor = 0;
            patch = 0;
            name = null;
        }
    }
}
