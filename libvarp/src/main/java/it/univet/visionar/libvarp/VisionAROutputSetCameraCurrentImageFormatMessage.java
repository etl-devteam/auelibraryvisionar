package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetCameraCurrentImageFormatMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 1;

    public VisionAROutputSetCameraCurrentImageFormatMessage(int index) {
        super(SET_CAM_IMG, TYPE_REQUEST);

        /* Check arguments */
        if (index < 0)
            throw new IllegalArgumentException("index setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.write(index);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
