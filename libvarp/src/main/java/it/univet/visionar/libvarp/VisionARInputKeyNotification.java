package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Date;

public class VisionARInputKeyNotification extends VisionARInputMessage {
    final int expPayloadSize = 14;

    private Date timeStamp;
    private int keyCode;
    private int keyValue;

    public VisionARInputKeyNotification(VisionARInputMessage msg) {
        super(msg);
    }

    public Date getDate() { return timeStamp; }
    public int getCode() { return keyCode; }
    public int getValue() { return keyValue; }

    @Override
    protected boolean checkHeader() {
        if ( (getType() == TYPE_NOTIFICATION)
                && ((getCommand() == NTF_CU_KEY) || (getCommand() == NTF_KEY))
                && (getPayloadSize() == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            int seconds = buffer.readInt();
            int nanoseconds = buffer.readInt();

            timeStamp = new Date((seconds*1000L) + (nanoseconds/1000000L));
            keyCode = buffer.readShort();
            keyValue = buffer.readInt();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
