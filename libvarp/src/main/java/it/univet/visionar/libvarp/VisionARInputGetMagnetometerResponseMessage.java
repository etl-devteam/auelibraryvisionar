package it.univet.visionar.libvarp;

import java.io.DataInputStream;


public class VisionARInputGetMagnetometerResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 13;

    private float magnetometerX;
    private float magnetometerY;
    private float magnetometerZ;

    public VisionARInputGetMagnetometerResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public float getMagnetometerX() { return magnetometerX; }
    public float getMagnetometerY() { return magnetometerY; }
    public float getMagnetometerZ() { return magnetometerZ; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_MAG_DATA)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            magnetometerX = buffer.readFloat();
            magnetometerY = buffer.readFloat();
            magnetometerZ = buffer.readFloat();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            magnetometerX = 0.0f;
            magnetometerY = 0.0f;
            magnetometerZ = 0.0f;
        }
    }
}
