package it.univet.visionar.libvarp;

import java.io.DataInputStream;


public class VisionARInputGetCompassResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 5;

    private float value;

    public VisionARInputGetCompassResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public float get() { return value; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_COMP_DATA)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            value = buffer.readFloat();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            value = 0.0f;
        }
    }
}
