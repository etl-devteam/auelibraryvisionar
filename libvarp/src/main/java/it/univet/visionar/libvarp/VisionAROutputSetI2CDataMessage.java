package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetI2CDataMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 8;

    public VisionAROutputSetI2CDataMessage(int addr, int reg, int size, int value) {
        super(SET_I2C_DATA, TYPE_REQUEST);

        /* Check arguments */
        if ((addr < 0) || (addr > 255))
            throw new IllegalArgumentException("address is out of range");
        if ((reg < 0) || (reg > 65535))
            throw new IllegalArgumentException("register is out of range");
        if ((size != 1) && (size != 2))
            throw new IllegalArgumentException("size is out of range");
        if ((value < 0) ||
                ((size == 1) && (value > 255)) ||
                ((size == 2) && (value > 65535)))
            throw new IllegalArgumentException("size is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.write(addr);
            stream.writeShort(reg);
            stream.write(size);
            stream.writeInt(value);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
