package it.univet.visionar.libvarp;

import java.io.DataInputStream;
import java.util.Date;

public class VisionARDisplayLockChangedNotification extends VisionARInputMessage {
    private final int expPayloadSize = 9;

    private Date timeStamp;
    private boolean status = false;

    public VisionARDisplayLockChangedNotification(VisionARInputMessage msg) {
        super(msg);
    }

    public Date getDate() { return timeStamp; }
    public boolean getStatus() { return status; }

    @Override
    protected boolean checkHeader() {
        if ( (getType() == TYPE_NOTIFICATION)
                && (getCommand() == NTF_DISPLAY_LOCK)
                && (getPayloadSize() == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            int seconds = buffer.readInt();
            int nanoseconds = buffer.readInt();

            timeStamp = new Date((seconds*1000L) + (nanoseconds/1000000L));
            status = buffer.readBoolean();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
