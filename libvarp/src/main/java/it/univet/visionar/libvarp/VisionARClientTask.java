package it.univet.visionar.libvarp;

import java.io.*;
import java.net.*;

import android.os.AsyncTask;


public class VisionARClientTask extends AsyncTask<VisionARClientTaskParams, Void, VisionARInputMessage> {

    private static final String TAG = "VARPCTSK";

    @Override
    protected VisionARInputMessage doInBackground(VisionARClientTaskParams... params) {
        Socket socket;
        DataOutputStream outToServer;
        DataInputStream inFromServer;
        VisionARInputMessage retMessage = new VisionARInputMessage();

        VisionARLog.v(TAG, params[0].ipAddr + " (" + params[0].port + ")");

        try {
            socket = new Socket(params[0].ipAddr, params[0].port);
            outToServer = new DataOutputStream(socket.getOutputStream());
            inFromServer = new DataInputStream(socket.getInputStream());
        }
        catch (Exception e) {
            e.printStackTrace();
            return retMessage;
        }

        VisionARLog.v(TAG, "connected!");

        try {
            params[0].message.writeTo(outToServer);
        }
        catch (Exception e) {
            e.printStackTrace();
            return retMessage;
        }

        try {
            retMessage.read(inFromServer);
        }
        catch (Exception e) {
            e.printStackTrace();
            retMessage.reset();
            return retMessage;
        }

        try {
            socket.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return retMessage;
    }

    @Override
    protected void onPostExecute(VisionARInputMessage result) {
        VisionARLog.i(TAG, "FROM SERVER:");
        VisionARLog.v(TAG, "* version: " + result.getVersion());
        VisionARLog.v(TAG, "* type: " + result.getType());
        VisionARLog.v(TAG, "* command: " + result.getCommand());
        VisionARLog.v(TAG, "* size: " + result.getPayloadSize());
        VisionARLog.v(TAG, "* response: " + result.getResponse());
    }
}
