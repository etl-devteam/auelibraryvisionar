package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetCameraBrightnessMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 4;

    public VisionAROutputSetCameraBrightnessMessage(int bright) {
        super(SET_CAM_BRIGHTNESS, TYPE_REQUEST);

        if ((bright < 0) || (bright > 255))
            throw new IllegalArgumentException("Contrast setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.writeInt(bright);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
