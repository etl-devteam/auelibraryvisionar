package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputGetI2CDataRequestMessage extends VisionAROutputMessage {

    public VisionAROutputGetI2CDataRequestMessage(byte addr, short index, int size) {
        super(GET_I2C_DATA, TYPE_REQUEST);

        /* Check arguments */
        // No need to check addr
        // No need to check index
        if ((size != 1) && (size != 2))
            throw new IllegalArgumentException("size is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(4);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.write(addr);
            stream.writeShort(index);
            stream.writeByte(size);
        }
        catch (Exception e) {
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), 4);
        }
        catch (Exception e) {
        }
    }
}
