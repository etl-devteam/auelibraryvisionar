package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetCUInfoResponseMessage extends VisionARInputMessage {
    private final static int expMinPayloadSize = 12;

    private int androidSdk;
    private int androidMajor;
    private int androidMinor;
    private int androidPatch;

    private int kernelRelease;
    private int kernelMajor;
    private int kernelMinor;

    private int fbMajor;
    private int fbMinor;

    private int usbMajor;
    private int usbMinor;

    private String board;

    public VisionARInputGetCUInfoResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int getAndroidSdk() { return androidSdk; }
    public int getAndroidMajorVersion() { return androidMajor; }
    public int getAndroidMinorVersion() { return androidMinor; }
    public int getAndroidPatchVersion() { return androidPatch; }
    public int getKernelReleaseVersion() { return kernelRelease; }
    public int getKernelMajorVersion() { return kernelMajor; }
    public int getKernelMinorVersion() { return kernelMinor; }
    public int getFbDriverMajorVersion() { return fbMajor; }
    public int getFbDriverMinorVersion() { return fbMinor; }
    public int getUsbDriverMajorVersion() { return usbMajor; }
    public int getUsbDriverMinorVersion() { return usbMinor; }
    public String getBoardName() { return board; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CU_INFO)
                && (size >= expMinPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            androidSdk = buffer.readUnsignedByte();
            androidMajor = buffer.readUnsignedByte();
            androidMinor = buffer.readUnsignedByte();
            androidPatch = buffer.readUnsignedByte();
            kernelRelease = buffer.readUnsignedByte();
            kernelMajor = buffer.readUnsignedByte();
            kernelMinor = buffer.readUnsignedByte();
            fbMajor = buffer.readUnsignedByte();
            fbMinor = buffer.readUnsignedByte();
            usbMajor = buffer.readUnsignedByte();
            usbMinor = buffer.readUnsignedByte();
            board = buffer.readLine();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            androidSdk = 0;
            androidMajor = 0;
            androidMinor = 0;
            androidPatch = 0;
            kernelRelease = 0;
            kernelMajor = 0;
            kernelMinor = 0;
            fbMajor = 0;
            fbMinor = 0;
            usbMajor = 0;
            usbMinor = 0;
            board = null;
        }
    }
}
