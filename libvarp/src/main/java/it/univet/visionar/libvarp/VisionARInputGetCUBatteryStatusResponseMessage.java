package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

public class VisionARInputGetCUBatteryStatusResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 4;

    private int internalBatteryLevel;
    private int externalBatteryLevel;
    private boolean localBatteryStatus;

    public VisionARInputGetCUBatteryStatusResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int getInternalBatteryLevel() { return internalBatteryLevel; }
    public int getExternalBatteryLevel() { return externalBatteryLevel; }
    public boolean getLocalBatteryStatus() { return localBatteryStatus; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CU_BATT_STATUS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            internalBatteryLevel = buffer.readUnsignedByte();
            externalBatteryLevel = buffer.readUnsignedByte();
            localBatteryStatus = buffer.readBoolean();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            internalBatteryLevel = 0;
            externalBatteryLevel = 0;
            localBatteryStatus = false;
        }
    }
}
