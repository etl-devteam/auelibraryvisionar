package it.univet.visionar.libvarp;

import java.io.DataInputStream;


public class VisionARInputGetConnStatusResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 2;

    private final static byte spectaclesMask = 0x01;
    private final static byte cameraMask = 0x02;

    private byte status;

    public VisionARInputGetConnStatusResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public boolean getSpectaclesStatus() { return ((status & spectaclesMask) != 0); }
    public boolean getCameraStatus() { return ((status & cameraMask) != 0); }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_CONN_STATUS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            status = buffer.readByte();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            status = 0;
        }
    }
}
