package it.univet.visionar.libvarp;

import java.io.DataInputStream;


public class VisionARInputGetAccelerometerResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 13;

    private float accelerometerX;
    private float accelerometerY;
    private float accelerometerZ;

    public VisionARInputGetAccelerometerResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public float getAccelerometerX() { return accelerometerX; }
    public float getAccelerometerY() { return accelerometerY; }
    public float getAccelerometerZ() { return accelerometerZ; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_ACC_DATA)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            accelerometerX = buffer.readFloat();
            accelerometerY = buffer.readFloat();
            accelerometerZ = buffer.readFloat();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            accelerometerX = 0.0f;
            accelerometerY = 0.0f;
            accelerometerZ = 0.0f;
        }
    }
}
