package it.univet.visionar.libvarp;

import java.io.DataInputStream;

public class VisionARInputGetBrightnessResponseMessage extends VisionARInputMessage {
    private final static int expPayloadSize = 2;

    private int value;

    public VisionARInputGetBrightnessResponseMessage(VisionARInputMessage msg) {
        super(msg);
    }

    public int get() { return value; }

    @Override
    protected boolean checkHeader() {
        if ( (type == TYPE_RESPONSE)
                && (cmd == GET_BRIGHTNESS)
                && (size == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            value = buffer.readUnsignedByte();
        } catch (Exception e) {
            VisionARLog.e(TAG, "error in reading data!");

            value = 0;
        }
    }
}
