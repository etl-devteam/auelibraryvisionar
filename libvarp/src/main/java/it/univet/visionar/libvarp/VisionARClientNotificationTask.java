package it.univet.visionar.libvarp;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Runnable;
import java.net.Socket;


public class VisionARClientNotificationTask implements Runnable {

    private static final String TAG = "VARP_NTF";

    private String ipAddress;
    private int port;

    private boolean stopFlag;   // flag to terminate socket reading loop

    public VisionARClientNotificationTask() {
        VisionARLog.i(TAG, VisionARMessage.defaultIpAddress + " (" + VisionARMessage.defaultTcpEvtPort + ")");

        this.ipAddress = VisionARMessage.defaultIpAddress;
        this.port = VisionARMessage.defaultTcpEvtPort;
    }

    public VisionARClientNotificationTask(String addr, int port) {
        VisionARLog.i(TAG, addr + " (" + port + ")");

        this.ipAddress = addr;
        this.port = port;
    }

    public void run () {
        Socket socket = null;
        InputStream socketStream = null;

        stopFlag = false;

        try {
            socket = new Socket(this.ipAddress, this.port);
            socketStream = socket.getInputStream();

            do {
                if (socket.isConnected()) {
                    this.update(new VisionARInputMessage(new DataInputStream(socketStream)));
                }
                else {
                    Thread.sleep(1000); // waiting for connection
                }
            } while (!stopFlag);

            socket.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        this.stopFlag = true;
    }

    protected void update(VisionARInputMessage message) {
        VisionARLog.i(TAG, "updated!");
    }
}
