package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Date;

public class VisionARConnectionStatusNotification extends VisionARInputMessage {
    private final int expPayloadSize = 9;

    private Date timeStamp;
    private byte status = 0;

    public VisionARConnectionStatusNotification(VisionARInputMessage msg) {
        super(msg);
    }

    public Date getDate() { return timeStamp; }
    public byte getStatus() { return status; }

    @Override
    protected boolean checkHeader() {
        if ( (getType() == TYPE_NOTIFICATION)
                && (getCommand() == NTF_CONN)
                && (getPayloadSize() == expPayloadSize) )
            return true;
        else
            return false;
    }

    @Override
    protected void readData(DataInputStream buffer) {
        try {
            int seconds = buffer.readInt();
            int nanoseconds = buffer.readInt();

            timeStamp = new Date((seconds*1000L) + (nanoseconds/1000000L));
            status = buffer.readByte();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
