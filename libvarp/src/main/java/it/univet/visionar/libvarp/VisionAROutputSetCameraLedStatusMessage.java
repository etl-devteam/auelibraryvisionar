package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetCameraLedStatusMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 1;

    public VisionAROutputSetCameraLedStatusMessage(int status) {
        super(SET_CAM_LED_STATUS, TYPE_REQUEST);

        /* Check arguments */
        if ((status != CAMERA_LED_OFF) && (status != CAMERA_LED_RED) && (status != CAMERA_LED_GREEN))
            throw new IllegalArgumentException("Status setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.write(status);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
