package it.univet.visionar.libvarp;


public class VisionARClientTaskParams {
    public String ipAddr;
    public int port;
    public VisionAROutputMessage message;

    public VisionARClientTaskParams(String ipAddr, int port, VisionAROutputMessage message) {
        this.ipAddr = ipAddr;
        this.port = port;
        this.message = message;
    }

    public VisionARClientTaskParams(VisionAROutputMessage message) {
        this.ipAddr = VisionARMessage.defaultIpAddress;
        this.port = VisionARMessage.defaultTcpPort;
        this.message = message;
    }
}
