package it.univet.visionar.libvarp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class VisionAROutputSetLEDsStatusMessage extends VisionAROutputMessage {
    private static final int expPayloadSize = 3;

    public VisionAROutputSetLEDsStatusMessage(int red, int green, int blue) {
        super(SET_LEDS_STATUS, TYPE_REQUEST);

        /* Check arguments */
        if ((red < 0) || (red > 255))
            throw new IllegalArgumentException("RED LED setting is out of range");
        if ((green < 0) || (green > 255))
            throw new IllegalArgumentException("GREEN LED setting is out of range");
        if ((blue < 0) || (blue > 255))
            throw new IllegalArgumentException("BLUE LED setting is out of range");

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(expPayloadSize);
        DataOutputStream stream = new DataOutputStream(buffer);
        try {
            stream.write(red);
            stream.write(green);
            stream.write(blue);
        }
        catch (Exception e) {
            /* nothing to do */
        }

        try {
            this.setPayload(new ByteArrayInputStream(buffer.toByteArray()), buffer.size());
        }
        catch (Exception e) {
            /* nothing to do */
        }
    }
}
